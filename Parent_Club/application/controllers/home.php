
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Home extends CI_Controller {

 function __construct()
 {
   parent::__construct();
 }

 function index()
 {
   if($this->session->userdata('logged_in'))
   {
     $session_data = $this->session->userdata('logged_in');

	 if($session_data['userType']==1){
		 $data['username'] = $session_data['username'];
		 $this->load->view('global/sadminheader',$data);
		 $this->load->view('sAdmin/superadmindashboard', $data);
		 $this->load->view('global/footer');
	 }elseif($session_data['userType']==2){
		 $data['username'] = $session_data['username'];
		 //var_dump($data['username']);die();
		 $this->load->view('global/adminheader',$data);
		 $this->load->view('admin/admindashboard', $data);
		 $this->load->view('global/footer');
	 }else{
		 $data['username'] = $session_data['username'];
		 $this->load->view('global/header');
		 $this->load->view('user/home_view', $data);
		 $this->load->view('global/footer');
	 }     
   }
   else
   {
     //If no session, redirect to login page
     redirect('login', 'refresh');
   }
 }

 function logout()
 {
  //print_r($this->session->unset_userdata('logged_in'));die(shailender);
   $this->session->unset_userdata('logged_in');
   session_destroy();
   redirect('home', 'refresh');
 }

}

?>

