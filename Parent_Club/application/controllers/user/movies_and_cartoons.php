<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Movies_And_Cartoons extends CI_Controller {

function __construct()
 {
   parent::__construct();
 }

 function index()
 {
	$this->load->helper(array('form'));
	$this->load->view('global/header');
	$this->load->view('user/movies_and_cartoons');
	$this->load->view('global/footer');
 }
}
?>
