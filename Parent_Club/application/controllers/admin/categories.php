
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categories extends CI_Controller {

 function __construct()
 {
   parent::__construct();
 }

 function index()
 {
   $this->load->helper(array('form'));
	$this->load->view('global/adminheader');
   $data['posts']=$this->category();
   $this->load->view('admin/categories',$data);
      $this->load->view('global/footer');

 }
 
	function category(){
			$sql=$this->db->query("select * from categories where is_show='1'");
			$result= $sql->result();			
			return $result;
			}
}

?>
