<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Administrators extends CI_Controller {

 function __construct()
 {
   parent::__construct();
 }

	function index(){
		$this->load->view('global/sadminheader');
		$data['posts']=$this->admini();
		$this->load->view('sAdmin/administrators',$data);
		$this->load->view('global/footer');

	}
	function admini(){
		$sql=$this->db->query("select * from users where userType='2'");
		$result= $sql->result();			
		return $result;
		}
}
?>
