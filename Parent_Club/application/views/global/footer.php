<footer id="footer">
				<div class="container">
					<div class="row">
                       <ul class="footer-nav">
                         <li><a href="#">Sitemap</a></li>
                         <li><a href="#">Privacy Policy</a></li>
                       </ul>
                       <p>&copy; Parent Club 2014. All Right Reserved.</p>
                    </div>
				</div>
			</footer>
		</div>

		<!-- Libs -->
		<script src="../../../ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="vendor/jquery.js"><\/script>')</script>
		<script src="<? echo base_url();?>js/bootstrap.js"></script>
		